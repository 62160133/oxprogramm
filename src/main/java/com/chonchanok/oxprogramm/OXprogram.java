/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.chonchanok.oxprogramm;
import java.util.Scanner;
/**
 *
 * @author Van
 */
public class OXprogram {
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
    	boolean win = false;
    	int turn = 0;
        int count = 0;
        int point1;
        int point2;
    	String[][] board=new String[3][3];
        System.out.println("Welcome to OX game ");
        System.out.println("\n  1 2 3");  
        for(int row = 0; row < board.length; row++){
        	 System.out.print(row+1+" ");
            for(int col = 0; col < board.length; col++){
                board[row][col] = "-";
                System.out.print(board[row][col]+" ");
            }
            System.out.println();
        }do {
        System.out.println("X turn");
        System.out.println("Please input Row Col : ");
        if (count % 2 == 0) {
        	point1 = kb.nextInt();
        	point2 = kb.nextInt(); 
        	if(board[point1 - 1][point2 - 1]=="-") {
            board[point1 - 1][point2 - 1] = "X";
            }else {
            	System.out.println("Please input Row Col again: ");
            	point1 = kb.nextInt();
            	point2 = kb.nextInt(); 
            	board[point1 - 1][point2 - 1] = "X";
            }
            System.out.println("\n  1 2 3");
            for (int row = 0; row < board.length; row++) {
                System.out.print(row + 1 + " ");
                for (int col = 0; col < board.length; col++) {
                    System.out.print(board[row][col] + " ");
                }
                System.out.println();
                
            }
            count++;
        }
        if (board[0][0].equals("X") && board[0][1].equals("X") && board[0][2].equals("X")) {
            System.out.println("Player X Win....");
            break;
        }
        else if (board[1][0].equals("X") && board[1][1].equals("X") && board[1][2].equals("X")) {
            System.out.println("Player X Win....");
            break;
        }
        else if (board[2][0].equals("X") && board[2][1].equals("X") && board[2][2].equals("X")) {
            System.out.println("Player X Win....");
            break;
        }
        else if (board[0][0].equals("X") && board[1][1].equals("X") && board[2][2].equals("X")) {
            System.out.println("Player X Win....");
            break;
        }
        else if (board[0][2].equals("X") && board[1][1].equals("X") && board[2][0].equals("X")) {
            System.out.println("Player X Win....");
            break;
        }
        else if (board[0][0].equals("X") && board[1][0].equals("X") && board[2][0].equals("X")) {
            System.out.println("Player X Win....");
            break;
        }
        else if (board[0][1].equals("X") && board[1][1].equals("X") && board[2][1].equals("X")) {
            System.out.println("Player X Win....");
            break;
        }
        else if (board[0][2].equals("X") && board[1][2].equals("X") && board[2][2].equals("X")) {
            System.out.println("Player X Win....");
            break;
        }
        turn++;
        if(turn == 9) {
        	win=true;
        	System.out.println("Player Draw");
        	break;
        }
        if (count % 2 != 0) {
            System.out.println("O turn");
            System.out.println("Please input Row Col : ");
            point1 = kb.nextInt();
            point2 = kb.nextInt();
        	if(board[point1 - 1][point2 - 1]=="-") {
                board[point1 - 1][point2 - 1] = "O";
                }else {
                	System.out.println("Please input Row Col again: ");
                	point1 = kb.nextInt();
                	point2 = kb.nextInt(); 
                	board[point1 - 1][point2 - 1] = "O";
                }
            System.out.println("\n  1 2 3");
            for (int row = 0; row < board.length; row++) {
                System.out.print(row + 1 + " ");
                for (int col = 0; col < board.length; col++) {
                    System.out.print(board[row][col] + " ");
                }
                System.out.println();
            }
            count++;
            if (board[0][0].equals("O") && board[0][1].equals("O") && board[0][2].equals("O")) {
                System.out.println("Player O Win....");
                break;
            }
            else if (board[1][0].equals("O") && board[1][1].equals("O") && board[1][2].equals("O")) {
                System.out.println("Player O Win....");
                break;
            }
            else if (board[2][0].equals("O") && board[2][1].equals("O") && board[2][2].equals("O")) {
                System.out.println("Player O Win....");
                break;
            }
            else if (board[0][0].equals("O") && board[1][1].equals("O") && board[2][2].equals("O")) {
                System.out.println("Player O Win....");
                break;
            }
            else if (board[0][2].equals("O") && board[1][1].equals("O") && board[2][0].equals("O")) {
                System.out.println("Player O Win....");
                break;
            }
            else if (board[0][0].equals("O") && board[1][0].equals("O") && board[2][0].equals("O")) {
                System.out.println("Player O Win....");
                break;
            }
            else if (board[0][1].equals("O") && board[1][1].equals("O") && board[2][1].equals("O")) {
                System.out.println("Player O Win....");
                break;
            }
            else if (board[0][2].equals("O") && board[1][2].equals("O") && board[2][2].equals("O")) {
                System.out.println("Player O Win....");
                break;
            }
            turn++;
            }
    } while (win!=true);
          System.out.println("Bye bye....");
    }
}
